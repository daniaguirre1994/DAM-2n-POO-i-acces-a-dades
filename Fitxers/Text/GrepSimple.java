package Text;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Created by Dani Aguirre.
 */
public class GrepSimple {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Introdueix la paraula a cercar: ");
        String paraula=sc.nextLine();
        Path path = Paths.get("/home/danii/Documentos/workspace/Exercises/src/Fitxers/src/Text/algo.txt");
        String s;
        try (BufferedReader lector = new BufferedReader(new FileReader(path.toString()))) {
            while ((s = lector.readLine()) != null) {
                if (s.contains(paraula))
                    System.out.println(s);
            }
        }catch (IOException e) {
            System.err.printf(e.getMessage());
        }

    }

}
