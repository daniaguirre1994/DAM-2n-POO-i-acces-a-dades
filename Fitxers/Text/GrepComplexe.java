package Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.*;
import java.util.Scanner;

/**
 * Created by Dani Aguirre.
 */
public class GrepComplexe {
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        System.out.println("Introdueix un fotxer o directori");
        String desti=sc.nextLine();
        System.out.println("Introdueix la paraula a cercar: ");
        String paraula=sc.nextLine();
        Path path = Paths.get(desti);

        if (Files.isDirectory(path)) {
            System.out.println("Es un directori");
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
                for (Path fitxer: stream) {
                        mostrar(fitxer,paraula);
                    }

            } catch (IOException | DirectoryIteratorException ex) {
                System.err.println(ex);
            }
        }else{
            if(Files.isRegularFile(path) && Files.isReadable(path))
                mostrar(path,paraula);
            else
                System.err.println("No es un fitxer");

        }
    }
    public static void mostrar(Path fitxer , String paraula){
    String s;
    if(Files.isRegularFile(fitxer) && Files.isReadable(fitxer)) {
        int linea = 0;
        try (BufferedReader lector = new BufferedReader(new FileReader(fitxer.toString()))) {
            while ((s = lector.readLine()) != null) {
                linea++;
                if (s.contains(paraula)) {
                    System.out.print("File: " + fitxer.getFileName());
                    System.out.println(" -> " + linea + " " + s);
                }
            }
        } catch (IOException e) {
            System.err.printf(e.getMessage());
        }
    }
    }

}
