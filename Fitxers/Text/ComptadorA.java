package Text;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dani Aguirre
 */
public class ComptadorA {
    public static void main(String[] args) {
        String nomFitxer = "/home/danii/Documentos/workspace/Exercises/src/Fitxers/src/Text/algo.txt";
        int lectura,cont=0;
        char ch;
        StringBuilder s = new StringBuilder();

        try (FileReader lector = new FileReader(nomFitxer)) {
            while ((lectura = lector.read())!=-1) {
                ch = (char) lectura;
                if (String.valueOf(ch).toUpperCase().equals("A"))
                    cont++;
                System.out.printf(String.valueOf(ch));

            }
            System.out.printf("\nLa lletra A apareix: "+cont+" vegades");

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
