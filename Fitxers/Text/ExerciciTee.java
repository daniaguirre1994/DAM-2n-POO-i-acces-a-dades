package Text;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Dani Aguirre.
 */
public class ExerciciTee {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String nomFitxer=sc.nextLine();
        String a=sc.nextLine();
        try (FileWriter escriptor =new FileWriter(nomFitxer)) {
                escriptor.write(a);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
