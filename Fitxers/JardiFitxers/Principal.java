package JardiFitxers;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * A la classe Principal creem un jardí, hi posem alguna planta,
 * i fem passar un torn cada cop que l'usuari introdueix una línia
 * de text.
 */
public class Principal {
	/**
	 * El jardí que es crea, de mida 40.
	 */
	private Jardi jardi;

	/**
	 * El main es limita a crear un objecte de tipus Principal. El
	 * constructor d'aquest objecte fa la resta. Això és habitual, com
	 * que treballem amb un objecte, evita haver de declarar la resta de
	 * propietats i mètodes de la classe com static.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new Principal();
	}

	/**
	 * S'inicialitza el jardí, i es demana entrada a l'usuari. Cada línia que
	 * s'introdueix avança un torn el jardí. Si l'usuari introdueix la cadena
	 * "surt" s'acaba el programa.
	 */
	public Principal() {
		Scanner sc = new Scanner(System.in);
		String ordre = "";

		if (!savedata()) {
			jardi = new Jardi(40);
			jardi.posaElement(new Altibus(), 10);
			jardi.posaElement(new Declinus(), 30);
		}

		while (!ordre.equals("surt")) {
			jardi.temps();
			System.out.println(jardi.toString());
			ordre = sc.nextLine();
		}
		sc.close();
		guardar();
	}
	private void guardar() {
		try (ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream("jardi.sav"))) {
			writer.writeObject(jardi);
			System.out.println("S'ha guardat la partida");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	private boolean savedata() {

		boolean dades = false;
		try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("jardi.sav"))) {
			jardi =(JardiFitxers.Jardi) lector.readObject();
			System.err.println("S'han restaurat les dades de la partida anterior");
			dades = true;
		} catch (IOException e) {
			System.err.println("No hi han dades guadades");
			dades=false;
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return dades;
	}



}
