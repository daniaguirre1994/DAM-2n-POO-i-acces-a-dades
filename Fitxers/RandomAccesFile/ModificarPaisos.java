package RandomAccesFile;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

/**
 * Created by Dani Aguirre.
 */
public class ModificarPaisos {
    private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
        StringBuilder b = new StringBuilder();
        char ch = ' ';
        for (int i=0; i<nChars; i++) {
            ch=fitxer.readChar();
            if (ch != '\0')
                b.append(ch);
        }
        return b.toString();
    }

    public static void main(String[] args) {
        String nom;
        int id, poblacio,opt,pos2 = 0;
        long pos=0;

        Scanner scanner = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);

        System.out.print("Introdueix número de registre: ");
        id=scanner.nextInt();
        scanner.nextLine();


        try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
            // per accedir a un registre multipliquem per la mida de
            // cada registre.
            pos=(id-1)*174;

            if (pos<0 || pos>=fitxer.length())
                throw new IOException("Número de registre invàlid.");

            fitxer.seek(pos);
            fitxer.readInt(); // Saltem l'id
            nom = readChars(fitxer, 40);
            String iso = readChars(fitxer, 3);
            String capital = readChars(fitxer, 40);
            poblacio = fitxer.readInt();
            System.out.println("País: "+nom+" ISO: "+iso+" Cap: "+capital+" Població: "+poblacio);
            System.out.print("Introdueix quina dada vols editar: 1-Nom , 2-Iso , 3-Capital , 4-Poblacio : ");
            opt=scanner.nextInt();
            switch (opt){
                case 1:
                    fitxer.seek(fitxer.getFilePointer()-170);
                    System.out.printf("Nom: ");
                    nom=sc.nextLine();
                    StringBuilder a=new StringBuilder(nom);
                    a.setLength(40);

                    fitxer.writeChars(a.toString());
                    //nom = readChars(fitxer, 40);
                    break;
                case 2:
                    fitxer.seek(fitxer.getFilePointer()-174+84);
                    System.out.printf("ISO: ");
                    iso=sc.nextLine();
                    if (!(iso.length()>3 || iso.length()<0)){
                        StringBuilder isoB=new StringBuilder(iso);
                        isoB.setLength(3);
                        fitxer.writeChars(isoB.toString());
                    }else
                        System.err.printf("El Iso es de 3 caracters");
                    break;
                case 3:

                    fitxer.seek(fitxer.getFilePointer()-174+90);
                    System.out.printf("Capital: ");
                    capital=sc.nextLine();
                    a=new StringBuilder(capital);
                    a.setLength(40);
                    fitxer.writeChars(a.toString());
                    break;
                case 4:
                    fitxer.seek(fitxer.getFilePointer()-4);
                    System.out.println("Introdueix la nova població: ");
                    poblacio = sc.nextInt();
                    sc.nextLine();
                    if (poblacio >= 0) {
                        fitxer.writeInt(poblacio);
                    } else {
                        System.err.println("La població ha de ser positiva.");
                    }
                    break;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        scanner.close();
    }
}