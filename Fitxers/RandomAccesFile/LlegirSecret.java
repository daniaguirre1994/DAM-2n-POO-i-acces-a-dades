package RandomAccesFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

/**
 * Created by Dani Aguirre.
 */
public class LlegirSecret {

    private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
        StringBuilder b = new StringBuilder();
        char ch = ' ';
        for (int i=0; i<nChars; i++) {
            ch=fitxer.readChar();
            if (ch != '\0')
                b.append(ch);
        }
        return b.toString();
    }

    public static void main(String[] args) {
        String nom = null;
        int id;
        long pos=0;

        Scanner scanner = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);

        System.out.print("Introdueix número de registre: ");
        id=scanner.nextInt();
        scanner.nextLine();


        try (RandomAccessFile fitxer = new RandomAccessFile("secret.bin", "rw")) {
           boolean trobat=false;
            pos=(id-1)*10;
            fitxer.seek(0);
            fitxer.seek(pos);
            pos=fitxer.readInt();
            String a=readChars(fitxer,3);
            System.out.println("Codi: "+pos+" lletres: "+a);
        }catch (FileNotFoundException e){
            System.err.println(e.getMessage());
        } catch (IOException e) {

            System.err.println(e.getMessage());
        }
        scanner.close();
    }
}
