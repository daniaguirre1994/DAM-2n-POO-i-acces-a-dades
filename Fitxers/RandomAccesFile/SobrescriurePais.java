package RandomAccesFile;

/**
 * Created by Dani Aguirre.
 */

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class SobrescriurePais {


    public static void main(String[] args) {
        String nom;
        int id, poblacio,opt,pos2 = 0;
        long pos=0;

        Scanner scanner = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);

        System.out.print("Introdueix número de registre: ");
        id=scanner.nextInt();
        scanner.nextLine();


        try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
            // per accedir a un registre multipliquem per la mida de
            // cada registre.
            pos=(id-1)*174;

            if (pos<0 || pos>=fitxer.length())
                throw new IOException("Número de registre invàlid.");
            fitxer.seek(pos);
            fitxer.readInt();

            System.out.print("Introdueix les noves dades: ");

            System.out.printf("Nom: ");
            nom=sc.nextLine();
            StringBuilder a=new StringBuilder(nom);
            a.setLength(40);
            fitxer.writeChars(a.toString());

            System.out.printf("ISO: ");
            String iso=sc.nextLine();
            if (!(iso.length()>3 || iso.length()<0)){
                StringBuilder isoB=new StringBuilder(iso);
                isoB.setLength(3);
                fitxer.writeChars(isoB.toString());
            }else
                System.err.printf("El Iso es de 3 caracters");

            System.out.printf("Capital: ");
            String capital=sc.nextLine();
            a=new StringBuilder(capital);
            a.setLength(40);
            fitxer.writeChars(a.toString());

            System.out.println("Introdueix la nova població: ");
            poblacio = sc.nextInt();
            sc.nextLine();
            if (poblacio >= 0) {
                fitxer.writeInt(poblacio);
            } else {
                System.err.println("La població ha de ser positiva.");
            }


        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        scanner.close();
        sc.close();
    }
}