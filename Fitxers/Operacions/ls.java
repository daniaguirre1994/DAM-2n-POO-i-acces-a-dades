package Operacions;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ls {

    public static void main(String[] args) { 
    	String permisos="";
            Path dir = Paths.get(System.getProperty("user.home"));
            System.out.println("Fitxers del directori " + dir);
            if (Files.isDirectory(dir)) {
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
                    for (Path fitxer: stream) {
                    	permisos="";
                    	permisos+= (Files.isDirectory(fitxer)) ? "D": "-";
                    	permisos+= (Files.isReadable(fitxer)) ? "R": "-";
                    	permisos+= (Files.isWritable(fitxer)) ? "W": "-";
                    	permisos+= (Files.isExecutable(fitxer)) ? "X": "-";
                        System.out.println(permisos+" "+fitxer.getFileName());
                    }
                } catch (IOException | DirectoryIteratorException ex) {         
                    System.err.println(ex);
                }
            } else {
                System.err.println(dir.toString()+" no �s un directori");
            }
}}