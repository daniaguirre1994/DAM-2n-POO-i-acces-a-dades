package Operacions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;
/**
 * Created by Dani Aguirre.
 */
public class copy {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.printf("Introdueix el nom dun fitxer");
        String fitxer=sc.nextLine();
        Path dir = Paths.get(System.getProperty("user.home"));
        Path file = Paths.get(fitxer);
        dir = dir.resolve(file.getFileName());
        try {
            Files.copy(file,dir, StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Fitxer: "+file.toString()+" copiat correctament a "+dir.toString());
    }
}
