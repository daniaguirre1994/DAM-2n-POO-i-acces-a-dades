package Operacions;

import java.io.IOException;
import java.nio.file.*;
import java.util.Deque;
import java.util.LinkedList;

public class lsR {

    public static void main(String[] args) {
        String permisos="";
        Deque<Path> pila = new LinkedList<Path>();
        Path dir = Paths.get(System.getProperty("user.home"));
//        Omplim la pila amb el 1r directori
        pila.push(dir);

        while (!pila.isEmpty()) {
            try {
                Path path=pila.pop();
                if (Files.isDirectory(path)) {
                    System.out.println("Fitxers del directori "+path);
                    try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
                        for (Path fitxer: stream) {
                            if (Files.isDirectory(fitxer))
                                pila.push(fitxer);
//                           Omplim la pila amb un directori fill i aixi amb cadascun

                            permisos="";
                            permisos+= (Files.isDirectory(fitxer)) ? "D": "-";
                            permisos+= (Files.isReadable(fitxer)) ? "R": "-";
                            permisos+= (Files.isWritable(fitxer)) ? "W": "-";
                            permisos+= (Files.isExecutable(fitxer)) ? "X": "-";
                            System.out.println(permisos+" "+fitxer.getFileName());
                        }
                    } catch (IOException | DirectoryIteratorException ex) {
                        System.err.println(ex);
                    }
                    System.out.println();
                } else
                    throw new IllegalArgumentException(dir+": No és un directori");
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}