package Binary;

import java.io.*;

/**
 * Created by Dani Aguirre.
 */
public class detectorText {
    public static void main(String[] args) {

        int b=0;
        char c;
        boolean eof=false;
        try (DataInputStream lector = new DataInputStream(new FileInputStream("1.bin"))) {

            while ((b = lector.readChar()) !=-1 && !eof) {
                c=(char)b;
                if(Character.isLetter(b))
                    System.out.print(c);
                else
                    System.out.print(" ");
            }
        }catch (EOFException e){
            eof=true;
        }catch (IOException e) {
            System.err.println("Error al crear el fitxer: " + e.getMessage());

        }

    }
}
