package exemple_encapsulacio;

/**
 *  Aquesta versió de Circumferencia és equivalent a la versió 1,
 *  però s'ha encapsulat correctament l'atribut àrea.
 */
public class CircumferenciaV3 {
	private final double area;
	public final double radi;
	public final double centreX;
	public final double centreY;

	public CircumferenciaV3(double radi) {
		this(0,0,radi);
	}

	public CircumferenciaV3(double centreX, double centreY) {
		this(centreX,centreY,1);
	}

	public CircumferenciaV3(double centreX, double centreY, double radi) {
		this.centreX = centreX;
		this.centreY = centreY;
		this.radi = radi;
		this.area = Math.PI * radi * radi;
	}

	public double getArea() {
		return area;
	}


}
