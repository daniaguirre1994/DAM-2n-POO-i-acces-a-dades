import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Dani Aguirre.
 */

public class Exe6 {
    public static void main(String[] args) {

        String num,num2;
        Scanner sc = new Scanner(System.in);

        System.out.println("Introdueix un num llarg: ");
        num = sc.nextLine();
        System.out.println("Introdueix un altre num llarg: ");
        num2 = sc.nextLine();
        Exe6 e1 = new Exe6(num);
        Exe6 e2 = new Exe6(num2);

        System.out.println("Primer num llarg: " + e1.toString()+"\nMida("+e1.mida()+")");
        System.out.println("Segon num llarg: " + e2.toString()+"\nMida("+e2.mida()+")");

        Exe6 prova=new Exe6();
        prova.suma(e1,e2);
        System.out.println("Suma dels dos nums llargs: " + prova.toString());
        sc.close();
    }
    //Declaracio de la clase
    private List<Integer> Exe6 = new ArrayList<Integer>();

    //Constructors
    public Exe6(long num) {
        while (num > 0) {
            addNum((int)num % 10);
            num /= 10;
        }
    }
    public Exe6(byte[] num) {
        for (int i = 0; i < num.length; i++) {
            Exe6.add((int)num[i]);
        }
    }
    public Exe6(String num) {
        char[] numero = num.toCharArray();
        for (int i = numero.length-1; i >= 0; i--)
            addNum(Integer.parseInt(""+numero[i]));
    }

    public Exe6() {}



    @Override
    public String toString() {
        String num = "";
        for (int i = 0; i < Exe6.size(); i++){
            num += Exe6.get(i);
        }
        return num;
    }
    //Getters
    public int getNum(int index) {
        return Exe6.get(index);
    }

    public int mida() {
        return Exe6.size();
    }

    private void addNum(int num) {
        Exe6.add(0, num);
    }

    public void suma(Exe6 e1, Exe6 e2) {
        int len1 = e1.mida()-1;
        int len2 = e2.mida()-1;
        int num1,num2,n;
        int x = 0;

        while (len1 >= 0 || len2 >= 0) {
            if (len1 < 0)num1 = 0;
            else num1 = e1.getNum(len1);
            if (len2 < 0)  num2 = 0;
            else num2 = e2.getNum(len2);
            n = num1 + num2 + x;
            x = 0;
            if (n >= 10) {
                x = n/10;
                n %= 10;
            }
            addNum(n);
            len1--;
            len2--;
        }
        if (x > 0)
            addNum(x);
    }

}
