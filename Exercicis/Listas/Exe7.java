import java.util.Scanner;

/**
 * Created by Dani Aguirre.
 */
public class Exe7 {
    public static void mostrar(LlistaOrdenada list){
        for (int i = 0; i < list.llistaOrdenada.size(); i++) {
            System.out.print(list.llistaOrdenada.get(i)+" ");
        }
    }
    public static void main(String[] args) {

        LlistaOrdenada list = new LlistaOrdenada();

        list.introduir(1);list.introduir(7);list.introduir(3);list.introduir(20);
        list.introduirBinary(45);list.introduirBinary(3);list.introduirBinary(1);list.introduirBinary(11);

        System.out.println("Lista Ordenada.");
        mostrar(list);

        list.suprimirBinary(1);
        list.suprimirBinary(20);

        System.out.println("Busca binaria d'un num\n Introdueix un numero: ");
        Scanner sc=new Scanner(System.in);
        int trobat = list.cercaBinaria(sc.nextInt());

        if(trobat>0)
            System.out.println("Numero trobat");
        else
            System.out.println("El num no esta");

    }
}
