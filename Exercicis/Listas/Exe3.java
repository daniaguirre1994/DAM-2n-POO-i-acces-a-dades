

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exe3 {

	public static void main(String[] args) {
		List<String> llenguatges=new ArrayList<String>();
		llenguatges.addAll(Arrays.asList("Java", "C", "C++", "Visual Basic", "PHP", "Python", "Perl", "C#", "JavaScript","Delphi"));
		System.out.println("Mes popular: "+llenguatges.get(0));
		System.out.println("6e Mes popular: "+llenguatges.get(5));
		for(int i=0; i<3;i++){
			System.out.print(i+1+"e Mes popular: "+llenguatges.get(i)+"\n");
		}
		for(int i=llenguatges.size()-1;i>llenguatges.size()-4;i--){
			System.out.print(i+1+"e Menys popular: "+llenguatges.get(i)+"\n");
		}
		System.out.println("Tots menys el primer");
		for(int i=1;i<llenguatges.size();i++){
			System.out.print(llenguatges.get(i)+", ");
		}
		System.out.println("\nTots\n------------");
		for(int i=0;i<llenguatges.size();i++){
			System.out.print(i+1+"e LLenguatge: "+llenguatges.get(i)+"\n");
		}
		System.out.println("Total: "+llenguatges.size());
	}

}
