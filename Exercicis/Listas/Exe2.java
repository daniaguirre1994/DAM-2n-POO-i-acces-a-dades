import java.util.*;

/**
 * Created by Dani Aguirre.
 */
public class Exe2 {
    public static void main(String[] args){
        List<Integer> lista=new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9));
        List<Integer> lista2=new ArrayList<Integer>(Arrays.asList(1,3,6,4,5,7,9,9));
        ordenada(lista);
        ordenada(lista2);
    }
    public static void ordenada(List<Integer> lista) {
        boolean isOrdenada=true;
        System.out.println(lista.toString());
        int len= lista.size()-1;
        for (int i = 0; i < len; i++){
            if (lista.get(i) > lista.get(i + 1)) isOrdenada= false;
        }
        if(isOrdenada)
            System.out.println("Ordenada");
        else
            System.out.println("No ordenada");

    }


}
