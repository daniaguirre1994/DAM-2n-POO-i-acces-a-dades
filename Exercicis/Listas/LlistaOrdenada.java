import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LlistaOrdenada {

	List<Integer> llistaOrdenada = new ArrayList<Integer>();
	//	Metode per introduir
	public void introduir(int num) {
		ListIterator<Integer> it = llistaOrdenada.listIterator();
		int i = 0;
		while (it.hasNext()){
			if (num > it.next()){
				i = it.nextIndex();
			}
		}
		llistaOrdenada.add(i, num);
	}
//	Metode per esborrar
	public void suprimir(int i) {
		Iterator<Integer> it = llistaOrdenada.iterator();
		while (it.hasNext()) {
			if (it.next() == i) {
				it.remove();
			}
		}
	}
	//	Metode de cerca binaria
	public int cercaBinaria(int num) {
		int i=-1,min=0;
		int max = llistaOrdenada.size()-1;
		int pos = 0;
		boolean trobat = false;
		while (min <= max && !trobat) {
			if (llistaOrdenada.get(pos) == num) {
				i = pos;
				trobat = true;
			}
			else if (llistaOrdenada.get(pos) < num)
				min = pos + 1;
			else if(llistaOrdenada.get(pos) > num)
				max = pos - 1;
			pos =(min + max)/2;
		}
		if (i == -1 && !trobat) {
			i = -1*(pos + 1);
		}
		return i;
	}
	//	Introduir obtenint la posicio per cerca binaria
	public void introduirBinary(int i) {
		int posicio = cercaBinaria(i);
		if (posicio < 0){
			posicio*=-1;
			llistaOrdenada.add((posicio),i);
		} else
			llistaOrdenada.add(posicio,i);
	}
//	Suprimir obtenint la posicio per cerca binaria
	public void suprimirBinary(int i){
		int posicio = cercaBinaria(i);
		if(posicio > 0)
			llistaOrdenada.remove(posicio);
	}
}