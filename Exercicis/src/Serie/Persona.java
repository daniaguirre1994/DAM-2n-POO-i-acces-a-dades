package Serie;

public class Persona implements Comparable{
	private int pes;
	private int edat;
	private int alcada;

	@Override
	public int compareTo(Object o) {
		Persona p =(Persona) o;
		return p.alcada-this.alcada;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public int getPes() {
		return pes;
	}

	public void setPes(int pes) {
		this.pes = pes;
	}
}
