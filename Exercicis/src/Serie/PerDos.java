package Serie;

public class PerDos implements Serie{
	private double n;
	@Override
	public void inicial(double x) {
		n=x;
	}

	@Override
	public void inicialitzar() {
		n=1;
	}

	@Override
	public double seguent() {
		n*=2;
		return n;
	}
}
