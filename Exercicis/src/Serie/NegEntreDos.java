package Serie;

public class NegEntreDos implements Serie{
	
	private double n=20;
	
	@Override
	public void inicial(double x) {
		n=x;
	}
	@Override
	public void inicialitzar() {
		n=30;
	}
	@Override
	public double seguent() {
		n=n/(-2);
		return n;
	}
	
}
