## Inserció de documents

```java
public class ExempleInsert {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("mascotes");

		coll.drop();

		Document document = new Document("nom", "Buffy")
				.append("edat", 3)
				.append("espècie", "gat");
		System.out.println(document.toJson());
		coll.insertOne(document);
		System.out.println(document.toJson());
		client.close();
	}
/*
> show databases
exemples  0.000GB
local     0.000GB
> use exemples
switched to db exemples
> show collections
mascotes
> db.mascotes.find()
{ "_id" : ObjectId("5695548fec24140c88a63bf1"), "nom" : "Buffy", "edat" : 3, "espècie" : "gat" }
>

 */
}
```
