## Consultes

```java
public class ExempleFind {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> collection = db.getCollection("punts");

		collection.drop();

		for (int x=0; x<5; x++) {
			for (int y=0; y<5; y++) {
				collection.insertOne(new Document("x",x).append("y", y));
			}
		}

		// Obtenir el primer element
		Document first = collection.find().first();
		System.out.println(first.toJson());
		System.out.println();
		// Recuperar tots els elements en una List
		List<Document> all = collection.find().into(new ArrayList<Document>());
		for (Document doc : all) {
			System.out.println(doc.toJson());
		}
		System.out.println();
		// Iterar per tots els elements sense guardar-los
		try (MongoCursor<Document> cursor = collection.find().iterator()) {
			while (cursor.hasNext()) {
				Document doc = cursor.next();
				System.out.println(doc.toJson());
			}
		}
		System.out.println();
		// Iterar per tots els elements, estil Java 8
		collection.find().forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		// Comptar la quantitat d'elements
		System.out.println(collection.count());

		client.close();
	}

}
```

### Filtres

```java
public class ExempleFiltres {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> collection = db.getCollection("punts");

		collection.drop();

		for (int x=0; x<5; x++) {
			for (int y=0; y<5; y++) {
				collection.insertOne(new Document("x",x).append("y", y));
			}
		}

		// db.punts.find({x:3})
		Bson filter = new Document("x", 3);
		List<Document> result = collection.find(filter).into(new ArrayList<Document>());
		for (Document doc : result) {
			System.out.println(doc.toJson());
		}
		System.out.println();

		// db.punts.find({x:{$lt:3},y:{$gte:3}})
		// Sintaxi basada en documents
		filter = new Document("x", new Document("$lt", 3))
				.append("y", new Document("$gte", 3));
		result = collection.find(filter).into(new ArrayList<Document>());
		for (Document doc : result) {
			System.out.println(doc.toJson());
		}
		// Sintaxi basada en Filters
		filter = Filters.and(Filters.lt("x", 2), Filters.gte("y", 3));
		// Igual que l'anterior amb els mètodes de Filters importats de format estàtica
		filter = and(lt("x", 2), gte("y", 3));

		client.close();
	}

}
```

### Projeccions

```java
public class ExempleProjection {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("punts");

		coll.drop();

		for (int i=0; i<100; i++) {
			int x = ThreadLocalRandom.current().nextInt(100);
			int y = ThreadLocalRandom.current().nextInt(100);
			int z = ThreadLocalRandom.current().nextInt(10);
			Document document = new Document("x",x).append("y", y).append("z", z);
			coll.insertOne(document);
		}

		Bson filter = and(gt("x", 50), eq("z", 5));
		// Retorna tots els elements, excepte que els s'indiqui amb un 0.
		// db.punts.find({$and : [{"x" : {$gt:50}}, {"z" : {$eq:5}}]}, {"x":0,"_id":0})
		Bson projection = new Document("x", 0).append("_id", 0);
		coll.find(filter).projection(projection).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		// Retorna només els elements indicats amb un 1, i l'_id.
		// db.punts.find({$and : [{"x" : {$gt:50}}, {"z" : {$eq:5}}]}, {"x":1})
		projection = new Document("x",1);
		coll.find(filter).projection(projection).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		// Retorna només els elements indicats amb un 1.
		// db.punts.find({$and : [{"x" : {$gt:50}}, {"z" : {$eq:5}}]}, {"y":1,"_id":0})
		projection = new Document("y", 1).append("_id", 0);
		coll.find(filter).projection(projection).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		// Equivalent al primer exemple
		projection = Projections.exclude("x","_id");
		coll.find(filter).projection(projection).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		// Equivalent al segon exemple
		projection = Projections.include("x");
		coll.find(filter).projection(projection).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		// Equivalent al tercer exemple
		projection = Projections.fields(Projections.include("y"), Projections.excludeId());
		coll.find(filter).projection(projection).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		client.close();
	}

}
```

### Ordenació i límits

```java
public class ExempleOrdenacio {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("punts");

		coll.drop();

		for (int i=0; i<100; i++) {
			int x = ThreadLocalRandom.current().nextInt(10);
			int y = ThreadLocalRandom.current().nextInt(10);
			int z = ThreadLocalRandom.current().nextInt(10);
			Document document = new Document("x",x).append("y", y).append("z", z);
			coll.insertOne(document);
		}

		Bson filter = eq("z", 5);
		Bson projection = fields(include("x","y"), excludeId());
		// Ordena primer per x de forma ascendent, i després per y de forma descendent.
		Bson sort = new Document("x", 1).append("y", -1);

		// db.punts.find({"z":{$eq:5}}, {"x":1,"y":1,"_id":0}).sort({"x":1,"y":-1})
		coll.find(filter).projection(projection).sort(sort).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		// Retornem només 5 resultats
		// db.punts.find({"z":{$eq:5}}, {"x":1,"y":1,"_id":0}).sort({"x":1,"y":-1}).limit(5)
		coll.find(filter).projection(projection).sort(sort).limit(5).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		// Retornem només 5 resultats i ens saltem els 3 primers resultats
		// db.punts.find({"z":{$eq:5}}, {"x":1,"y":1,"_id":0}).sort({"x":1,"y":-1}).skip(3).limit(5)
		coll.find(filter).projection(projection).sort(sort).skip(3).limit(5).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
		System.out.println();

		// Clàusula sort equivalent
		sort = Sorts.orderBy(Sorts.ascending("x"), Sorts.descending("y"));

		// I amb imports static:
		sort = orderBy(ascending("x"), descending("y"));

		// Si només volem ordenar per un camp:
		sort = ascending("x");

		// O per més d'un camp, però tots ascendents/descendents:
		sort = ascending("x","y");


		client.close();

	}

}
```
