## Exercicis

### Format JSON

#### Exercici 1

Tradueix el següent fragment XML (extret de
http://www.w3schools.com/xml/cd_catalog.xml) a JSON.

```xml
<CATALOG>
  <CD>
    <TITLE>Empire Burlesque</TITLE>
    <ARTIST>Bob Dylan</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>Columbia</COMPANY>
    <PRICE>10.90</PRICE>
    <YEAR>1985</YEAR>
  </CD>
  <CD>
    <TITLE>Hide your heart</TITLE>
    <ARTIST>Bonnie Tyler</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>CBS Records</COMPANY>
    <PRICE>9.90</PRICE>
    <YEAR>1988</YEAR>
  </CD>
</CATALOG>
```

#### Exercici 2

Tradueix el següent fragment XML (extret de http://json.org/example.html) a
JSON.

```xml
<menu id="file" value="File">
  <popup>
    <menuitem value="New" onclick="CreateNewDoc()" />
    <menuitem value="Open" onclick="OpenDoc()" />
    <menuitem value="Close" onclick="CloseDoc()" />
  </popup>
</menu>
```

#### Exercici 3

Tradueix el següent fragment JSON (extret de ) a XML.

```json
{
  "glossary": {
    "title": "example glossary",
		"GlossDiv": {
      "title": "S",
			"GlossList": {
        "GlossEntry": {
          "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
            "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
          },
					"GlossSee": "markup"
        }
      }
    }
  }
}
```
