**POO i accés a dades**

M6UF3. persistència en BD natives XML
=====================================

- [Introducció a MongoDB](introduccio.md)
- [Format JSON](json.md)
- [Introducció a Gradle](gradle.md)
- [Representació de documents](document.md)
- [Inserció de documents](insercio.md)
- [Consultes](consultes.md)
- [Modificació de dades](modificacio.md)
- [Eliminació de documents](eliminacio.md)
- [Aggregation framework](aggregation.md)
- [Exercicis](exercicis.md)
