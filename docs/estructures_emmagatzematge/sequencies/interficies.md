#### Interfícies

##### Interfície *Collection*

Si examinem l'estructura d'interfícies que descriuen les col·leccions
d'elements en Java, trobem el següent diagrama:

![](../imatges/collection.png)

És a dir, hi ha una interfície *Collection* que descriu les operacions
bàsiques que tenen la major part de col·leccions, i les interfícies com
*Set*, *Queue* o *List* amplien aquestes operacions bàsiques per a
proporcionar els diferents tipus de col·leccions que existeixen.
Recordeu que estem parlant de moment només d'interfícies: aquí encara no
hi ha cap implementació concreta, només es descriuen els mètodes que
aquestes estructures han de tenir.

És interessant examinar ara quines són les operacions que es descriuen a
*Collection*
(<http://download.oracle.com/javase/6/docs/api/java/util/Collection.html>).

##### Interfície Queue

Les piles i les cues són dues estructures dinàmiques especialment
senzilles, que ens permetran introduir la major part d'idees
relacionades amb la implementació d'estructures dinàmiques.

Una **cua** és una seqüència d'elements en què els elements s'afegeixen per
un dels extrems i s'extreuen per l'altre:

![Esquema cua](../imatges/queue.png)

L'element 1 és el que fa més temps que s'ha posat a la cua i serà el
primer a sortir quan s'extregui un element. Aquest tipus d'estructura
també s'anomena **FIFO** (*First In, First Ou*t).

Les cues s'utilitzen en aquelles situacions en què no es poden processar
tots els elements que ens arriben prou ràpidament, i se'ls posa en
espera. Per exemple, la cua d'impressió de documents per una impressora
permet que hi hagi diversos documents a l'espera de ser impresos, i que
es processin en el mateix ordre com han anat arribant.

L'operació d'inserció a una pila o a una cua s'anomena **push**, mentre que
l'operació d'extracció s'anomena **pop**.

Una cua ha de proporcionar com a mínim les operacions per inserir un
element, per extreure un element, per saber quants elements hi ha a la
cua, i per consultar sense extreure l'element que hi ha al davant. En
Java, la interfície *Queue* declara tots aquests mètodes, a més dels que
ja hi havia a *Collection*. Cadascun dels mètodes hi és per duplicat: la
versió que, si una operació no es pot realitzar (*add*, *remove*,
*element*), llança una excepció, i la versió que retorna un valor
especial (*offer*, *poll*, *peek*).

##### Interfície Deque

En el cas de les **piles**, els nous elements s'afegeixen sempre per una de les
bandes de la seqüència, i s'extreuen per la mateixa banda:

![Esquema pila](../imatges/stack.png)

En aquesta figura, l'element 1 és el primer que s'ha introduït a la
pila, fins al 5 que és l'últim. En canvi, l'element 5 serà el primer que
s'extraurà de la pila, i l'1 l'últim.

A les piles també se'ls anomena **estructures LIFO** (*Last In, First Out*),
perquè l'últim element que ha entrat serà el primer element que sortirà.

Com podem veure, en una pila (o una cua) no es pot accedir a qualsevol
element de la seqüència. Per poder accedir a l'element 4, primer cal
haver extret l'element 5. Fixeu-vos que això mateix és el que passa amb
la crida de funcions en un llenguatge de programació: si la funció *a()*
ha cridat a la funció *b()*, i *b()* ha cridat a la funció *c()*, primer
cal retornar de *c()* i després de *b()*, no es pot retornar de *c()* a
*a()* directament:

![Pila de funcions](../imatges/function_stack.png)

Les adreces de retorn de cadascuna de les funcions s'han emmagatzemat en una
pila, i cada vegada que s'arriba a un *return*, s'extreu una adreça de
la pila i es prossegueix l'execució a partir d'allà.

A les API de Java hi ha la classe *Stack* que implementa una pila.
Aquesta classe, però, prové de les primeres versions del llenguatge, i
actualment no es recomana el seu ús. En comptes d'això, s'ha creat la
interfície *Deque*, que amplia la interfície *Queue,* de manera que es
podria dir que aquesta interfície és la unió d'una cua i una pila: es
poden inserir i extreure elements per qualsevol dels dos extrems de la
seqüència.

Els mètode de *Deque* es resumeixen a la taula següent, segons el tipus
d'operació i de si actuen sobre un extrem o l'altre:

<table>
<tr><th></th><th colspan="2">Primer elements (cap)</th><th colspan="2">Últim element (cua)</th></tr>
<tr><td></td><td>Llença excepció</td><td>Valor especial</td><td>Llença excepció</td><td>Valor especial</td></tr>
<tr><td>**Inserir**</td><td>*addFirst*</td><td>*offerFirst*</td><td>*addLast*, *add*</td><td>*offerLast*, *offer*</td></tr>
<tr><td>**Extreure**</td><td>*removeFirst*, *remove*</td><td>*pollFirst*, *poll*</td><td>*removeLast*</td><td>*pollLast*</td></tr>
<tr><td>**Examina**</td><td>*getFirst*, *element*</td><td>*peekFirst*, *peek*</td><td>*getLast*</td><td>*peekLast*</td></tr>
</table>

##### Interfície List

Una **llista** és una seqüència d'elements en la qual es pot inserir o
extreure un element a qualsevol posició, i es pot accedir a qualsevol
dels seus elements. Una llista és molt similar a un vector, amb
l'excepció que canvia la seva mida quan s'afegeixen o s'extreuen
elements.

És important no confondre el concepte de llista amb la implementació de
llista enllaçada. La llista és una estructura dinàmica amb unes certes
característiques i que permet una sèrie d'operacions, a nivell
conceptual, i que es pot implementar de diverses maneres: com a llista
enllaçada o com a vector dinàmic.

En Java, el concepte d'una llista ve descrit a la interfície *List* (del
paquet *java.util*, no confondre amb la classe *List* de *java.awt* que
és un control gràfic).

A més de les operacions de *Collection*, una *List* tindrà operacions
per afegir, extreure, obtenir i intercanviar elements (*add*, *remove*,
*get*, *set*) especificant la posició de l'element afectat, i operacions
per obtenir la posició que ocupa un element dins de la llista (*indexOf*
i *lastIndexOf*), entre d'altres.
