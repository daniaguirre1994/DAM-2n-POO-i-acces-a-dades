## Sentències DML. Select

Un cop hem establert la connexió a la base de dades podem començar a enviar-hi
sentències SQL.

Per fer això en JDBC hem d'obtenir un objecte *Statement* a partir de la
connexió. L'objecte *Statement* ens permet enviar sentències SQL a la base de
dades i recuperar el resultat.

Al resultat d'una consulta SQL s'hi pot accedir utilitzant un *ResultSet*.
Aquest *ResultSet* és un cursor: podem passar a la següent fila amb el
mètode *next()* i recuperar les dades de cadascuna de les files amb les
diferents variants dels mètodes *get*, com *getInt()* o *getString()*.

El següent exemple mostra per pantalla les dades dels 10 primers empleats
(alfabèticament):

```java
public class CrearConsulta {
	public static void main(String[] args) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		String url = "jdbc:mysql://localhost:3306/employees";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connectionProperties.setProperty("password", "usbw");
		try (Connection con = DriverManager.getConnection(url,connectionProperties);
				Statement st = con.createStatement();) {
			System.out.println("Base de dades connectada!");
			try (ResultSet rs = st.executeQuery("SELECT * FROM employees ORDER BY last_name, first_name LIMIT 10")) {
				System.out.println("Dades de la taula employees:");
				while (rs.next()) {
					int empNo = rs.getInt("emp_no");
					LocalDate birthDate = rs.getObject("birth_date", LocalDate.class);
					String firstName = rs.getString("first_name");
					String lastName = rs.getString("last_name");
					Gender gender = Gender.valueOf(rs.getString("gender"));
					LocalDate hireDate = rs.getObject("hire_date", LocalDate.class);
					System.out.println(empNo+"\t"+birthDate.format(formatter)+"\t"+
							firstName+"\t"+lastName+"\t"+gender+hireDate.format(formatter));
				}
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}

	public enum Gender {
		M, F;
	}
}
```

De l'exemple anterior hi ha dos aspectes a comentar: el tractament de les
dates i el tractament de les enumeracions.

La traducció entre els tipus de temps de SQL al tipus de temps en Java sempre
ha estat complicada.

A partir de Java 8 es millora la correspondència entre els tipus de data i
temps SQL i els de Java. Per llegir dades d'aquests tipus no cal utilitzar el
mètode *getDate()* que s'utilitzava en versions anteiors, sinó que tots es
poden llegir amb el mètode genèric *getObject()*.

La correspondència entre tipus SQL i tipus Java és la següent:

| SQL | Java |
|----------|------|
| DATE | LocalDate |
| TIME | LocalTime |
| TIMESTAMP | LocalDateTime |
| TIME amb fus horari | OffsetTime |
| TIMESTAMP amb fus horariWITH TIMEZONE | OffsetDateTime |

El mètode *getObject()* rep el nom o posició de l'atribut a recuperar i el
tipus d'objecte que volem. Per exemple:

```java
LocalDate date = rs.getObject("date", LocalDate.class);
```

A l'exemple hem utilitzat un *DateTimeFormatter* per donar format a les dates
a l'hora de mostrar-les per pantalla. L'hem declarat així:

```java
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
```

Això indica que volem veure les dates posant primer el dia, després el mes i
després l'any, i que separem cada camp amb guions.

Per utilitzar el *formatter* hem fet `birthDate.format(formatter)`, cosa
que ens retorna un *String* amb les dades de la data *birthDate* expressades
en el format que volem.

Per altra banda, el MySQL té un tipus de dada enumeració que no és present a
altres SGBD. JDBC els recupera directament com String. Si volem mantenir el
tipus enumeració, podem crear un *enum* amb els mateixos valors, i utilitzar
el mètode *valueOf* de l'enum, que tradueix una cadena de text al valor
corresponent de l'*enum*. Així s'ha fet a l'exemple.

El següent exemple és similar al primer, però accedim a les columnes
retornades a través del seu índex en comptes del seu nom:

```java
public class CrearConsulta2 {
	public static void main(String[] args) {
		boolean hasResults=false;
		String url = "jdbc:mysql://localhost:3306/employees";
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("user", "root");
		connectionProperties.setProperty("password", "usbw");
		try (Connection con = DriverManager.getConnection(url,connectionProperties);
				Statement st = con.createStatement()) {
			System.out.println("Connexió a la base de dades!");
			try (ResultSet rs = st.executeQuery("SELECT first_name, last_name FROM employees ORDER BY last_name, first_name LIMIT 10")) {
				while (rs.next()) {
					if (!hasResults) {
						hasResults=true;
						System.out.println("Llista d'empleats:");
					}
					String firstName = rs.getString(1);
					String lastName = rs.getString(2);
					System.out.println(firstName+" "+lastName);
				}
				if (!hasResults) {
					System.out.println("No hi ha dades a la taula");
				}
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}
}
```

El següent exemple mostra una situació una mica més complexa. A la BD
d'empleats, cada empleat pot tenir un o més títols. Volem un programa que
ens mostri tots els títols que hi ha a la base de dades, que permeti a
l'usuari triar-ne un, i que finalment mostri tots els empleats que tenen el
títol triat per l'usuari.

```java
public class CrearConsulta3 {
	private Connection connection = null;
	private Statement statement = null;
	private Scanner scanner= new Scanner(System.in);

	private void connect() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/employees";
		String user = "root";
		String passwd = "usbw";
		connection = DriverManager.getConnection(url, user, passwd);
		statement = connection.createStatement();
	}

	private void disconnect() {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				;
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				;
			}
		}
	}

	public List<String> getTitles() throws SQLException {
		List<String> titles = new ArrayList<String>();
		try (ResultSet rs = statement.executeQuery("SELECT title FROM titles;")) {
			//Afegeix tots els títols a la llista
			while (rs.next()) {
				titles.add(rs.getString(1));
			}
		}
		return titles;
	}

	public String chooseTitle(List<String> titles) {
		String title = "";
		for(String t : titles) {
			System.out.println(t);
		}
		System.out.println("\nQuin títol vols? ");
		while (!titles.contains(title)) {
			title = scanner.nextLine();
			if (!titles.contains(title))
				System.out.println("Aquest títol no existeix.");
		}
		return title;
	}

	public void employeesByTitle(String title) throws SQLException {
		try (ResultSet rs = statement.executeQuery("SELECT first_name, last_name "
				  + "FROM employees JOIN titles "
				  + "ON employees.emp_no=titles.emp_no "
				  + "WHERE title LIKE '" + title + "'")
		) {
			while(rs.next()) {
				System.out.println(rs.getString("first_name")+" "+
						rs.getString("last_name"));
			}
		}
	}

	public void run() {
		String title;
		try {
			//1- Connecta a la bbdd
			connect();
			//2- Mostra els títols i demana a l'usuari que en triï un
			title = chooseTitle(getTitles());
			//3- Mostra els empleats que tenen el títol seleccionat
			System.out.println("Títol seleccionat: "+title);
			employeesByTitle(title);			
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			disconnect();
		}
	}

	public static void main(String[] args) {
		CrearConsulta3 ex = new CrearConsulta3();
		ex.run();
	}
}
```

Hi ha un aspecte important a destacar en aquest exemple:

El títol a cercar a la base de dades l'introdueix l'usuari, i l'incorporem a
una sentència SQL dins del mètode *employeesByTitle*.

Per tant, qualsevol caràcter especial que l'usuari hagi introduït s'incorporarà
a la sentència SQL i serà interpretar segons les regles del SQL.

Això és un problema greu de seguretat. Un usuari hàbil pot contruir una
expressió que faci que s'executin sentències al SGBD molt diferents a les que
teníem previstes. Amb aquesta tècnica, coneguda com **SQL-Injection**, es poden
obtenir dades qualsevol de la BD, i es poden modificar o eliminar registres
als quals se suposa que l'usuari no ha de tenir accés.

Encara que l'usuari no ho faci intencionadament, pot incloure algun caràcter
com unes cometes o similar a la seva cerca i això farà que el programa
funcioni malament.

Una opció que tenim per evitar aquest problema és filtrar qualsevol entrada que
vingui de fora del programa (sigui introduïda per un usuari, transmesa per
una xarxa, o obtinguda d'un fitxer). En aquest filtre podríem eliminar els
caràcters que tenen un significat especial en SQL.

Fer això, però, és delicat: és fàcil oblidar-se de filtrar algunes de les
entrades en un programa complex, o d'equivocar-se en algun detall del filtre.

La solució passa per fer que sigui el propi JDBC qui filtri les entrades
insegures. Això ho farem utilitzant una *PreparedStatement* en comptes d'un
*Statement*, tal com veurem al següent apartat.
