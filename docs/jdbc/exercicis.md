## Exercicis

Aquests exercicis utilitzen la base de dades
[Sakila](https://dev.mysql.com/doc/index-other.html). Pots veure aquí la
seva [estructura](https://dev.mysql.com/doc/sakila/en/sakila-structure.html).

### Consultes estàtiques

#### Exercici 1

Fes un programa que mostri el nom i descripció de totes les pel·lícules que
duren més de dues hores i mitja.

#### Exercici 2

Fes un programa que mostri el nom i cognom dels actors que apareixen a la
pel·lícula TWISTED PIRATES.

#### Exercici 3

Fes un programa que mostri quins ítems i de quines botigues té en lloguer el
client de nom ALLISON STANLEY. També haurà de mostrar la data de retorn
d'aquests ítems.

#### Exercici 4

Fes un programa que mostri totes les botigues que hi ha, amb les seves adreces
i el nom i cognom del seu encarregat.

#### Exercici 5

Fes un programa que localitzi els clients que tenen pel·lícules que ja haurien
d'haver retornat. Volem el nom i cognom i el telèfon dels clients i el títol
de la pel·lícula que deuen.

#### Exercici 6

Fes un programa que mostri les diferents categories de pel·lícules que tenim
i quantes pel·lícules de cada categoria hi ha.

### Consultes amb paràmetres

Aquests exercicis s'han de resoldre utilitzant sentències preparades.

#### Exercici 7

Fes un programa que demani a l'usuari el cognom d'un actor i que mostri per
pantalla totes les seves pel·lícules.

#### Exercici 8

Modifica l'exercici 3 per demanar a l'usuari el nom i cognom del client que
s'ha de mostrar.

#### Exercici 9

Fes un programa que permeti cercar un client i ens mostri tots els pagaments
que ha fet entre dues dates donades. De cada pagament es mostrarà la
quantitat, la data, i el nom de l'ítem que s'havia llogat (si és el cas,
hi ha pagaments que no estan associats a cap ítem).

#### Exercici 10

Fes un programa que permeti fer cerques de pel·lícules per paraula clau. El
programa ha de cercar la paraula clau en el títol i en la descripció.

Per cada pel·lícula trobada, s'ha d'indicar a quines botigues es pot trobar.

#### Exercici 11

Fes un programa que permeti als usuaris consultar les pel·lícules
disponibles en una botiga. Primer, es mostraran totes les botigues, amb la
seva adreça.

Un cop seleccionada una botiga, l'usuari podrà realitzar cerques pel nom de
les pel·lícules, pel nom dels actors o pel gènere de les pel·lícules.

El resultat d'aquestes cerques mostrarà el títol de les pel·lícules d'aquesta
botiga que compleixin els requisits, i si estan disponibles, o si no ho
estan, la seva data de retorn.

A través de la llista de pel·lícules, se'n pot seleccionar una, i se'n
mostraran els detalls: títol, descripció, any de producció, nacionalitat
i actors que hi apareixen.

Separa la interacció amb l'usuari de la gestió de la base de dades, de manera
que aquest codi sigui reaprofitable.
